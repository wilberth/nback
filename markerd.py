#!/usr/bin/env python3
#export FLASK_APP=markerd; export FLASK_ENV=development; python3 -m flask run
import time
from flask import Flask
from flask_cors import CORS
from rusocsci import buttonbox

bb = buttonbox.Buttonbox()
print(bb._device)
bb.sendMarker(val=128)

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello_world():
	bb.sendMarker(val=1)
	time.sleep(1)
	bb.sendMarker(val=0)
	return '"<p>Hello, World!</p>"'
